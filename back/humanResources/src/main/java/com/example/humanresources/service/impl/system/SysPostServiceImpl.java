package com.example.humanresources.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.humanresources.entity.system.SysPost;
import com.example.humanresources.entity.system.SysRole;
import com.example.humanresources.mapper.system.SysPostMapper;
import com.example.humanresources.service.system.SysPostService;
import com.example.humanresources.vo.system.SysPostQueryVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Jim
* @description 针对表【sys_post(岗位信息表)】的数据库操作Service实现
* @createDate 2024-01-11 20:06:12
*/
@Service
public class SysPostServiceImpl extends ServiceImpl<SysPostMapper, SysPost>
    implements SysPostService {

    @Resource
    SysPostMapper sysPostMapper;

    public List<SysPost> selectPostInfo(SysPostQueryVo sysPostQueryVo) {
        return sysPostMapper.selectPostInfo(sysPostQueryVo);
    }
}





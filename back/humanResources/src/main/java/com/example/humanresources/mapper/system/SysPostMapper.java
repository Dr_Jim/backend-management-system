package com.example.humanresources.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysPost;
import com.example.humanresources.vo.system.SysPostQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_post(岗位信息表)】的数据库操作Mapper
* @createDate 2024-01-11 20:06:12
* @Entity generator.domain.SysPost
*/
public interface SysPostMapper extends BaseMapper<SysPost> {
    List<SysPost> selectPostInfo(@Param("sysPostQueryVo") SysPostQueryVo sysPostQueryVo);
}





package com.example.humanresources.vo.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysDictDataQueryVo  implements Serializable {

    private static final long serialVersionUID = 1L;

    private Integer page;
    private Integer limit;

    @ApiModelProperty(value = "字典类型")
    @TableField("dict_type")
    private String dictType;
    @ApiModelProperty(value = "字典标签")
    @TableField("dict_label")
    private String dictLabel;
    @ApiModelProperty(value = "状态")
    @TableField("status")
    private String status;

}

package com.example.humanresources.vo.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysPostQueryVo implements Serializable {
    private static final long serialVersionUID = 1L;

    private Integer page;
    private Integer limit;

    @ApiModelProperty(value = "岗位id")
    @TableField("postId")
    private Long postId;
}

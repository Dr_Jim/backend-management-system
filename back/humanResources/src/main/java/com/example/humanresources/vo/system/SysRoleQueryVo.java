//
//
package com.example.humanresources.vo.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 角色查询实体
 * </p>
 *
 * @author qy
 * @since 2019-11-08
 */

@Data
public class SysRoleQueryVo implements Serializable {

	private static final long serialVersionUID = 1L;

	private Integer page;
	private Integer limit;

	@ApiModelProperty(value = "roleId")
	@TableField("roleId")
	private Long roleId;
//
//	@ApiModelProperty(value = "角色名称")
//	@TableField("role_name")
//	private String roleName;
//
//	@ApiModelProperty(value = "角色编码")
//	@TableField("role_code")
//	private String roleCode;
//
//	@ApiModelProperty(value = "描述")
//	@TableField("description")
//	private String description;

}


package com.example.humanresources.service.common;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysRole;
import com.example.humanresources.vo.system.SysRoleMergeVo;

public interface FileService extends IService<SysRole> {
    int insertRoleInfo(SysRoleMergeVo sysRoleMergeVo);
}

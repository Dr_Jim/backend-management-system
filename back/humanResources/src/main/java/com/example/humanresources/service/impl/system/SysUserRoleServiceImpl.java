package com.example.humanresources.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.humanresources.entity.system.SysUserRole;
import com.example.humanresources.mapper.system.SysUserRoleMapper;
import com.example.humanresources.service.system.SysUserRoleService;
import org.springframework.stereotype.Service;

/**
* @author Jim
* @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Service实现
* @createDate 2024-01-12 20:57:23
*/
@Service
public class SysUserRoleServiceImpl extends ServiceImpl<SysUserRoleMapper, SysUserRole>
    implements SysUserRoleService {

}





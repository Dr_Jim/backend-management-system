package com.example.humanresources.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.humanresources.entity.system.SysRole;
import com.example.humanresources.mapper.system.SysRoleMapper;
import com.example.humanresources.service.system.SysRoleService;
import com.example.humanresources.vo.system.SysRoleMergeVo;
import com.example.humanresources.vo.system.SysRoleQueryVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Jim
* @description 针对表【sys_role(角色信息表)】的数据库操作Service实现
* @createDate 2024-01-02 08:56:15
*/
@Service
public class SysRoleServiceImpl extends ServiceImpl<SysRoleMapper, SysRole>
    implements SysRoleService {
    @Resource
    SysRoleMapper sysRoleMapper;

    /**
     *  查询所有角色信息
     * @param sysRoleQueryVo 用户角色信息
     * @return List
     */
    @Override
    public List<SysRole> selectRoleInfo(SysRoleQueryVo sysRoleQueryVo) {
        return sysRoleMapper.selectRoleInfo(sysRoleQueryVo);
    }

    /**
     * 删除角色信息
     * @param deleteId
     * @return int
     */
    @Override
    public int deleteRoleInfo(String deleteId) {
        return sysRoleMapper.deleteRoleInfo(deleteId);
    }

    /**
     * 添加角色信息
     * @param sysRoleMergeVo 用户角色信息
     * @return List
     */
    @Override
    public int insertRoleInfo(SysRoleMergeVo sysRoleMergeVo) {
        return sysRoleMapper.insertRoleInfo(sysRoleMergeVo);
    }

    /**
     * 更新角色信息
     * @param sysRoleMergeVo
     * @return int
     */
    @Override
    public int updateRoleInfo(SysRoleMergeVo sysRoleMergeVo) {
        return sysRoleMapper.updateRoleInfo(sysRoleMergeVo);
    }
}





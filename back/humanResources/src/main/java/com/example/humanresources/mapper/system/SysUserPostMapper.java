package com.example.humanresources.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysUserPost;
import com.example.humanresources.vo.system.SysUserMergeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;


/**
* @author Jim
* @description 针对表【sys_user_post(用户与岗位关联表)】的数据库操作Mapper
* @createDate 2024-01-12 20:38:04
* @Entity generator.domain.SysUserPost
*/

@Mapper
public interface SysUserPostMapper extends BaseMapper<SysUserPost> {

    public int insertUserPost(@Param("userPostList") List<SysUserPost> userPostList);

}





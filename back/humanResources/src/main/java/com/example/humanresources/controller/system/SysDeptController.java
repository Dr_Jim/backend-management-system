package com.example.humanresources.controller.system;


import com.example.humanresources.config.PageBean;
import com.example.humanresources.enumeration.ResultCodeEnum;
import com.example.humanresources.mapper.system.SysDeptMapper;
import com.example.humanresources.service.system.SysDeptService;
import com.example.humanresources.util.Result;
import com.example.humanresources.entity.system.SysDept;
import com.example.humanresources.vo.system.SysDeptMergeVo;
import com.example.humanresources.vo.system.SysDeptQueryVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.sql.Wrapper;
import java.util.List;

@Api(tags = "部门管理接口")
@RestController
@RequestMapping("/system/dept")
public class SysDeptController {

    @Resource
    private SysDeptService sysDeptService;


    @ApiOperation("查询部门信息")
    @PostMapping("selectDept")
    public Result selectDept(@RequestBody SysDeptQueryVo sysDeptQueryVo){
        // 设置分页
        PageHelper.startPage(sysDeptQueryVo.getPage(), sysDeptQueryVo.getLimit());
        List<SysDept> list = sysDeptService.selectDeptInfo(sysDeptQueryVo);
        System.out.println(list);
        // 查询用户部门列表（需要分页的查询）
//        PageInfo<SysDept> pageInfo = new PageInfo<SysDept>(list);
        PageBean<SysDept> result = new PageBean<>(list);
        return Result.ok(result);
    }


    @ApiOperation("添加部门信息")
    @PostMapping("insertDept")
    public Result insertDept(@RequestBody SysDeptMergeVo sysDeptMergeVo) {
        int flag = sysDeptService.insertDeptInfo(sysDeptMergeVo);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }

    @ApiOperation("更新部门信息")
    @PostMapping("updateDept")
    public Result updateDept(@RequestBody SysDeptMergeVo sysDeptMergeVo) {
        int flag = sysDeptService.updateDeptInfo(sysDeptMergeVo);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }


    @ApiOperation("删除部门信息")
    @PostMapping("deleteDept/{deleteId}")
    public Result deleteDept(@PathVariable String deleteId) {
        if (deleteId.isEmpty()){
            return Result.build(null, ResultCodeEnum.SELECT_DATA_ERROR);
        }
        int flag = sysDeptService.deleteDeptInfo(deleteId);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }

}

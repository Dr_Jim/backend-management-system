package com.example.humanresources.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysUser;
import com.example.humanresources.vo.system.SysUserMergeVo;
import com.example.humanresources.vo.system.SysUserQueryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

@Mapper
public interface SysUserMapper extends BaseMapper<SysUser> {
    List<SysUser> selectUserInfo(@Param("entity") SysUserQueryVo sysUserQueryVo);

    int insertUserInfo(@Param("SysUserMergeVo") SysUserMergeVo sysUserQueryVo);
    int updateUserInfo(@Param("SysUserMergeVo") SysUserMergeVo sysUserQueryVo);
    int deleteUserInfo(@Param("deleteIds") List<String> deleteIds);

    List<SysUser> selectLastUserId();
}

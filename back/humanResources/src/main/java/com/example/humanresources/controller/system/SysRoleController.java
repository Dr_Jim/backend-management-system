package com.example.humanresources.controller.system;

import com.example.humanresources.config.PageBean;
import com.example.humanresources.entity.system.SysDept;
import com.example.humanresources.entity.system.SysRole;
import com.example.humanresources.enumeration.ResultCodeEnum;
import com.example.humanresources.service.system.SysRoleService;
import com.example.humanresources.util.Result;
import com.example.humanresources.vo.system.SysRoleMergeVo;
import com.example.humanresources.vo.system.SysRoleQueryVo;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@Api(tags = "角色管理接口")
@RestController
@RequestMapping("/system/role")
public class SysRoleController {

    @Resource
    private SysRoleService sysRoleService;


    @ApiOperation("查询角色信息")
    @PostMapping("selectRole")
    public Result selectRole(@RequestBody SysRoleQueryVo sysRoleQueryVo){
        // 设置分页必填参数
        if (sysRoleQueryVo.getPage() == null){
            sysRoleQueryVo.setPage(1);
        }
        if (sysRoleQueryVo.getLimit() == null){
            sysRoleQueryVo.setLimit(10);
        }
        // 设置分页
        PageHelper.startPage(sysRoleQueryVo.getPage(), sysRoleQueryVo.getLimit());
        List<SysRole> list = sysRoleService.selectRoleInfo(sysRoleQueryVo);
        // 查询用户角色列表（需要分页的查询）
//        PageInfo<SysRole> pageInfo = new PageInfo<SysRole>(list);
        PageBean<SysRole> result = new PageBean<>(list);
        return Result.ok(result);
    }

    @ApiOperation("添加角色信息")
    @PostMapping("insertRole")
    public Result insertRole(@RequestBody SysRoleMergeVo sysRoleMergeVo) {
        int flag = sysRoleService.insertRoleInfo(sysRoleMergeVo);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }

    @ApiOperation("更新角色信息")
    @PostMapping("updateRole")
    public Result updateRole(@RequestBody SysRoleMergeVo sysRoleMergeVo) {
        int flag = sysRoleService.updateRoleInfo(sysRoleMergeVo);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }


    @ApiOperation("删除角色信息")
    @PostMapping("deleteRole/{deleteId}")
    public Result deleteRole(@PathVariable String deleteId) {
        if (deleteId.isEmpty()){
            return Result.build(null, ResultCodeEnum.SELECT_DATA_ERROR);
        }
        int flag = sysRoleService.deleteRoleInfo(deleteId);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }

}

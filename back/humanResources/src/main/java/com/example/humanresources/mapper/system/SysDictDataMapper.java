package com.example.humanresources.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysDictData;
import com.example.humanresources.vo.system.SysDictDataQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_dict_data(字典数据表)】的数据库操作Mapper
* @createDate 2024-01-03 20:14:48
* @Entity generator.domain.SysDictData
*/
public interface SysDictDataMapper extends BaseMapper<SysDictData> {
    /**
     * 根据条件分页查询字典数据
     *
     * @param sysDictDataQueryVo 字典数据信息
     * @return 字典数据集合信息
     */
    List<SysDictData> selectDictDataList(@Param("sysDictDataQueryVo") SysDictDataQueryVo sysDictDataQueryVo);
}





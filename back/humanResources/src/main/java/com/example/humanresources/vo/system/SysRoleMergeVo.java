package com.example.humanresources.vo.system;


import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;

@Data
public class SysRoleMergeVo implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "roleId")
    @TableField("roleId")
    private Long roleId;

    @ApiModelProperty(value = "角色名称")
    @TableField("role_name")
    private String roleName;

    @ApiModelProperty(value = "角色状态")
    @TableField("status")
    private String status;

    @ApiModelProperty(value = "描述")
    @TableField("remark")
    private String remark;
}

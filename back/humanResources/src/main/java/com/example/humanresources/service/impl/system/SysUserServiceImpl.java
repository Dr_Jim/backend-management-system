package com.example.humanresources.service.impl.system;


import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.humanresources.entity.system.SysUser;
import com.example.humanresources.entity.system.SysUserPost;
import com.example.humanresources.entity.system.SysUserRole;
import com.example.humanresources.mapper.system.SysUserMapper;
import com.example.humanresources.mapper.system.SysUserPostMapper;
import com.example.humanresources.mapper.system.SysUserRoleMapper;
import com.example.humanresources.service.system.SysUserService;
import com.example.humanresources.vo.system.SysUserMergeVo;
import com.example.humanresources.vo.system.SysUserQueryVo;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {

    @Resource
    SysUserMapper sysUserMapper;

    @Resource
    SysUserPostMapper sysUserPostMapper;

    @Resource
    SysUserRoleMapper sysUserRoleMapper;


    /**
     * 查询所有用户信息
     * @param sysUserQueryVo 用户查询信息
     */
    @Override
    public List<SysUser> selectUserInfo(SysUserQueryVo sysUserQueryVo) {
//        查出所有user的postid
        List<SysUserPost> userPostList = sysUserPostMapper.selectList(null);
        List<SysUser> userList = sysUserMapper.selectUserInfo(sysUserQueryVo);
        for (SysUserPost sysUserPost: userPostList) {

        }
        return sysUserMapper.selectUserInfo(sysUserQueryVo);
    }

    public List<SysUser> selectLastUserId(){
        return sysUserMapper.selectLastUserId();
    }

//    回滚防止脏数据
    @Transactional(rollbackFor = Exception.class)
    @Override
    public int insertUserInfo(SysUserMergeVo sysUserMergeVo) {
////        查出最后的userid +1添加
//        List<SysUser> sysUsers = selectLastUserId();
//        Long userId = sysUsers.get(0).getUserId();
//        sysUserMergeVo.setUserId(userId != null ? userId + 1 : 1);
//        System.out.println(sysUserMergeVo.getUserId());
//        int rows = sysUserMapper.insertUserInfo(sysUserMergeVo);
////        添加成功后关联岗位和角色 即插入中间表数据
//        insertUserPost(sysUserMergeVo);
//        insertUserRole(sysUserMergeVo);
//        return rows;
        return 0;
    }

    @Override
    public int updateUserInfo(SysUserMergeVo sysUserQueryVo) {
//        如果前端没有传则为其补全数据
        return sysUserMapper.updateUserInfo(sysUserQueryVo);
    }

    @Override
    public int deleteUserInfo(List<String> deleteIds) {
        return sysUserMapper.deleteUserInfo(deleteIds);
    }

    public void insertUserPost(SysUserMergeVo sysUserMergeVo){
//        获取前端传来的岗位数组
        List<Long> postIds = sysUserMergeVo.getPostIds();
//        创建一个上述数组长度的list 存储对象
        if(!postIds.isEmpty()){
            ArrayList<SysUserPost> list = new ArrayList<>(postIds.size());
            for (Long postId: postIds) {
                SysUserPost sysUserPost = new SysUserPost();
                sysUserPost.setUserId(sysUserMergeVo.getUserId());
                sysUserPost.setPostId(postId);
                list.add(sysUserPost);
            }
            //        传递给mapper
            sysUserPostMapper.insertUserPost(list);
        }

    }

    public void insertUserRole(SysUserMergeVo sysUserMergeVo){
        List<Long> roleIds = sysUserMergeVo.getRoleIds();
        if(!roleIds.isEmpty()){
            ArrayList<SysUserRole> list = new ArrayList<>(roleIds.size());
            for (long roleId: roleIds) {
                SysUserRole sysUserRole = new SysUserRole();
                sysUserRole.setUserId(sysUserMergeVo.getUserId());
                sysUserRole.setRoleId(roleId);
                list.add(sysUserRole);
            }
            sysUserRoleMapper.insertUserRole(list);
        }
    }




//    @Override
//    public Result login(LoginVo loginVo) {
//
//        Result<Object> result = new Result<>();
//
////        1.校验密码不能为空
//        if (loginVo.getUsername() == null || loginVo.getPassword() == null) {
//            return Result.build(null, ResultCodeEnum.ARGUMENT_ERROR);
//        }
////        判断用户是否存在
//        QueryWrapper queryWrapper = new QueryWrapper();
//        queryWrapper.eq("username", loginVo.getUsername());
//        SysUser sysUser = sysUserMapper.selectOne(queryWrapper);
//        if(sysUser == null){
//            return Result.build(null,ResultCodeEnum.ACCOUNT_ERROR);
//        }
////        2.密码MD5加密
//        String md5Password = DigestUtils.md5DigestAsHex(loginVo.getPassword().getBytes());
////          判断用户是否相等
//        String password = sysUser.getPassword();
//        if (!md5Password.equals(password)){
//            return Result.build(null,ResultCodeEnum.PASSWORD_ERROR);
//        }
////        3.密码相等返回用户信息 看情况是否脱敏
//        Result.build(sysUser,ResultCodeEnum.PASSWORD_ERROR);
//        return result;
//    }

}


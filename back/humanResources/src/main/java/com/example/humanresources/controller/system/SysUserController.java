package com.example.humanresources.controller.system;

import com.example.humanresources.config.PageBean;
import com.example.humanresources.entity.system.SysUser;
import com.example.humanresources.enumeration.ResultCodeEnum;
import com.example.humanresources.service.system.SysUserService;
import com.example.humanresources.util.Result;
import com.example.humanresources.vo.system.SysUserMergeVo;
import com.example.humanresources.vo.system.SysUserQueryVo;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@RestController
@RequestMapping("/system/user")
public class SysUserController {

    @Resource
    private SysUserService sysUserService;

//    @ApiOperation("用户登录")
//    @PostMapping("login")
//    public Result login(@RequestBody LoginVo loginVo ){
//        return sysUserService.login(loginVo);
//    }

//
//    @ApiOperation("用户登录")
//    @PostMapping("login2")
//    public Result login2(){
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("token", "admin-token");
//        return Result.ok(map);
//    }

//    @ApiOperation("用户信息")
//    @GetMapping("info")
//    public Result info(){
//        HashMap<String, Object> map = new HashMap<>();
//        map.put("roles", "[admin]");
//        map.put("introduction", "I am a super administrator");
//        map.put("avatar", "https://oss.youlai.tech/youlai-boot/2023/05/16/811270ef31f548af9cffc026dfc3777b.gif");
//        map.put("name", "super admin");
//        return Result.ok(map);
//    }


    @ApiOperation("查询所有用户信息")
    @PostMapping("selectUserInfo")
    public Result selectUserInfo(@RequestBody SysUserQueryVo sysUserQueryVo){
        // 设置分页必填参数
        if (sysUserQueryVo.getPage() == null){
            sysUserQueryVo.setPage(1);
        }
        if (sysUserQueryVo.getLimit() == null){
            sysUserQueryVo.setLimit(10);
        }
        PageHelper.startPage(sysUserQueryVo.getPage(), sysUserQueryVo.getLimit());
        List<SysUser> list = sysUserService.selectUserInfo(sysUserQueryVo);
        // 查询用户角色列表（需要分页的查询）
        PageBean<SysUser> result = new PageBean<>(list);
        return Result.ok(result);
    }

    @ApiOperation("添加所有用户信息")
    @PostMapping("insertUserInfo")
    public Result insertUserInfo(@RequestBody SysUserMergeVo sysUserMergeVo) {
        int flag = sysUserService.insertUserInfo(sysUserMergeVo);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }

    @ApiOperation("更新所有用户信息")
    @PostMapping("updateUserInfo")
    public Result updateUserInfo(@RequestBody SysUserMergeVo sysUserMergeVo) {
        int flag = sysUserService.updateUserInfo(sysUserMergeVo);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }


    @ApiOperation("删除所有用户信息")
    @PostMapping("deleteUserInfo")
    public Result deleteUserInfo(@RequestBody List<String> deleteIds) {
        System.out.println(deleteIds);
        if (deleteIds.isEmpty()){
            return Result.build(null, ResultCodeEnum.SELECT_DATA_ERROR);
        }
        int flag = sysUserService.deleteUserInfo(deleteIds);
        if(flag > 0){
            return Result.ok();
        }
        return Result.fail();
    }


    @ApiOperation("导出用户信息")
    @PostMapping("export")
    public void exportUserInfo(HttpServletResponse httpServletResponse, @RequestBody Object downloadParams) throws IOException {
        System.out.println("导出用户信息" + downloadParams);
    }

}

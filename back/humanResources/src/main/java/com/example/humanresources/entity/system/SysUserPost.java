package com.example.humanresources.entity.system;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

import java.io.Serializable;

/**
 * 用户与岗位关联表
 * @TableName sys_user_post
 */
@TableName(value ="sys_user_post")
@Data
public class SysUserPost implements Serializable {
    /**
     * 用户ID
     */
//    @TableId(value = "user_id")
    private Long userId;

    /**
     * 岗位ID
     */
//    @TableId(value = "post_id")
    private Long postId;

    @TableField(exist = false)
    private static final long serialVersionUID = 1L;
}
package com.example.humanresources.controller.system;

import com.example.humanresources.entity.system.SysDictData;
import com.example.humanresources.service.system.SysDictDataService;
import com.example.humanresources.util.Result;

import com.example.humanresources.vo.system.SysDictDataQueryVo;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/system/dict/data")
public class SysDictDataController {

    @Resource
    private SysDictDataService sysdictDataService;

    @PostMapping("/list")
    public Result List(@RequestBody SysDictDataQueryVo sysDictDataQueryVo) {
        List<SysDictData> list = sysdictDataService.selectDictDataList(sysDictDataQueryVo);
        return Result.ok(list);
    }
}

package com.example.humanresources.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysRole;
import com.example.humanresources.vo.system.SysRoleMergeVo;
import com.example.humanresources.vo.system.SysRoleQueryVo;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_role(角色信息表)】的数据库操作Mapper
* @createDate 2024-01-02 08:56:15
* @Entity generator.domain.SysRole
*/
public interface SysRoleMapper extends BaseMapper<SysRole> {
    List<SysRole> selectRoleInfo(@Param("sysRoleQueryVo") SysRoleQueryVo sysRoleQueryVo);

    int deleteRoleInfo(@Param("deleteId") String deleteId);

    int insertRoleInfo(@Param("sysRoleMergeVo") SysRoleMergeVo sysRoleMergeVo);

    int updateRoleInfo(@Param("sysRoleMergeVo") SysRoleMergeVo sysRoleMergeVo);
}





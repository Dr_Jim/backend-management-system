package com.example.humanresources.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysPost;
import com.example.humanresources.vo.system.SysPostQueryVo;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_post(岗位信息表)】的数据库操作Service
* @createDate 2024-01-11 20:06:12
*/
public interface SysPostService extends IService<SysPost> {

    List<SysPost> selectPostInfo(SysPostQueryVo sysPostQueryVo);
}

package com.example.humanresources.vo.system;


import lombok.Data;

import java.io.Serializable;

/**
 * <p>
 * 用户查询实体
 * </p>
 */
@Data
public class SysUserQueryVo implements Serializable {
	
	private static final long serialVersionUID = 1L;

	private Integer page;

	private Integer limit;

	private String userName;

	private String phoneNumber;

	private String status;

	private Integer deptId;

	private String userId;

}


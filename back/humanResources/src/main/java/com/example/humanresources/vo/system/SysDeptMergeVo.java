package com.example.humanresources.vo.system;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;

@Data
public class SysDeptMergeVo implements Serializable {

    private static final long serialVersionUID = 1L;
//    @NotBlank(message = "id不能为空")
    @ApiModelProperty(value = "dept_id")
    @TableField("dept_id")
    private Long deptId;

    @ApiModelProperty(value = "父部门id")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty(value = "祖级列表")
    @TableField("ancestors")
    private Long ancestors;

    @ApiModelProperty(value = "部门名称")
    @TableField("dept_name")
    private String deptName;

    @ApiModelProperty(value = "显示顺序")
    @TableField("order_num")
    private Long orderNum;

    @ApiModelProperty(value = "部门负责人")
    @TableField("leader")
    private String leader;

    @ApiModelProperty(value = "电话")
    @TableField("phone")
    private String phone;

    @ApiModelProperty(value = "邮件")
    @TableField("email")
    private String email;

    @ApiModelProperty(value = "备注")
    @TableField("description")
    private String description;

}

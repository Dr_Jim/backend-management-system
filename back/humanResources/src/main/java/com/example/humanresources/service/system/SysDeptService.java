package com.example.humanresources.service.system;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysDept;
import com.example.humanresources.vo.system.SysDeptMergeVo;
import com.example.humanresources.vo.system.SysDeptQueryVo;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_dept(组织机构)】的数据库操作Service
* @createDate 2023-11-24 21:42:14
*/
public interface SysDeptService extends IService<SysDept> {
    int insertDeptInfo(SysDeptMergeVo sysDeptMergeVo);

    List<SysDept> selectDeptInfo(SysDeptQueryVo sysDeptQueryVo);

    int deleteDeptInfo(String deleteId);

    int updateDeptInfo(SysDeptMergeVo sysDeptMergeVo);
}

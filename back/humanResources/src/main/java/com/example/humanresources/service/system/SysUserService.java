package com.example.humanresources.service.system;


import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysUser;
import com.example.humanresources.vo.system.SysUserMergeVo;
import com.example.humanresources.vo.system.SysUserQueryVo;

import java.util.List;

public interface SysUserService extends IService<SysUser> {
//    Result login(LoginVo loginVo);

    List<SysUser> selectUserInfo(SysUserQueryVo sysUserQueryVo);

    int insertUserInfo(SysUserMergeVo sysUserMergeVo);

    int updateUserInfo(SysUserMergeVo sysUserMergeVo);

    int deleteUserInfo(List<String> deleteIds);
}

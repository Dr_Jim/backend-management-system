package com.example.humanresources.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysUserPost;


/**
* @author Jim
* @description 针对表【sys_user_post(用户与岗位关联表)】的数据库操作Service
* @createDate 2024-01-12 20:38:04
*/
public interface SysUserPostService extends IService<SysUserPost> {

}

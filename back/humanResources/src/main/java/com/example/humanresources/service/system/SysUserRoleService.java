package com.example.humanresources.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysUserRole;

/**
* @author Jim
* @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Service
* @createDate 2024-01-12 20:57:23
*/
public interface SysUserRoleService extends IService<SysUserRole> {

}

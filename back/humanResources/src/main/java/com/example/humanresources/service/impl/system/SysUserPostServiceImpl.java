package com.example.humanresources.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.humanresources.entity.system.SysUserPost;
import com.example.humanresources.mapper.system.SysUserPostMapper;
import com.example.humanresources.service.system.SysUserPostService;
import org.springframework.stereotype.Service;

/**
* @author Jim
* @description 针对表【sys_user_post(用户与岗位关联表)】的数据库操作Service实现
* @createDate 2024-01-12 20:38:04
*/
@Service
public class SysUserPostServiceImpl extends ServiceImpl<SysUserPostMapper, SysUserPost>
    implements SysUserPostService {

}





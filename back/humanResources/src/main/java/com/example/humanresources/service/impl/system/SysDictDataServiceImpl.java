package com.example.humanresources.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.example.humanresources.entity.system.SysDictData;
import com.example.humanresources.mapper.system.SysDictDataMapper;
import com.example.humanresources.service.system.SysDictDataService;
import com.example.humanresources.vo.system.SysDictDataQueryVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Jim
* @description 针对表【sys_dict_data(字典数据表)】的数据库操作Service实现
* @createDate 2024-01-03 20:14:48
*/
@Service
public class SysDictDataServiceImpl extends ServiceImpl<SysDictDataMapper, SysDictData>
    implements SysDictDataService {

    @Resource
    private SysDictDataMapper sysDictDataMapper;

    /**
     * 根据条件分页查询字典数据
     *
     * @param sysDictDataQueryVo 字典数据信息
     * @return 字典数据集合信息
     */
    @Override
    public List<SysDictData> selectDictDataList(SysDictDataQueryVo sysDictDataQueryVo)
    {
        return sysDictDataMapper.selectDictDataList(sysDictDataQueryVo);
    }

}





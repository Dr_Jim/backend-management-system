package com.example.humanresources.mapper.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysDept;
import com.example.humanresources.vo.system.SysDeptMergeVo;
import com.example.humanresources.vo.system.SysDeptQueryVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_dept(组织机构)】的数据库操作Mapper
* @createDate 2023-11-24 21:42:14
* @Entity generator.domain.SysDept
*/

@Mapper
public interface SysDeptMapper extends BaseMapper<SysDept> {
    List<SysDept> selectDeptInfo(@Param("sysDeptQueryVo") SysDeptQueryVo sysDeptQueryVo);

    int deleteDeptInfo(@Param("deleteId") String deleteId);

    int insertDeptInfo(@Param("sysDeptMergeVo") SysDeptMergeVo sysDeptMergeVo);

    int updateDeptInfo(@Param("sysDeptMergeVo") SysDeptMergeVo sysDeptMergeVo);
}





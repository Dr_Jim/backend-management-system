package com.example.humanresources.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.example.humanresources.entity.base.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import javax.validation.constraints.NotBlank;
import java.util.Date;

@Data
@ApiModel(description = "部门")
@TableName("sys_dept")
public class SysDept {
	/**
	 * 部门id
	 */
	@TableId(value = "dept_id", type = IdType.AUTO)
	private Long deptId;

	/**
	 * 父部门id
	 */
	@TableField(value = "parent_id")
	private Long parentId;

	/**
	 * 祖级列表
	 */
	@TableField(value = "ancestors")
	private String ancestors;

	/**
	 * 部门名称
	 */
	@TableField(value = "dept_name")
	private String deptName;

	/**
	 * 显示顺序
	 */
	@TableField(value = "order_num")
	private Integer orderNum;

	/**
	 * 负责人
	 */
	@TableField(value = "leader")
	private String leader;

	/**
	 * 联系电话
	 */
	@TableField(value = "phone")
	private String phone;

	/**
	 * 邮箱
	 */
	@TableField(value = "email")
	private String email;

	/**
	 * 部门状态（0正常 1停用）
	 */
	@TableField(value = "status")
	private String status;
	/**
	 * 备注
	 */
	@TableField(value = "description")
	private String description;

	/**
	 * 删除标志（0代表存在 2代表删除）
	 */
	@TableField(value = "del_flag")
	private String delFlag;

	/**
	 * 创建者
	 */
	@TableField(value = "create_by")
	private String createBy;

	/**
	 * 创建时间
	 */
	@TableField(value = "create_time")
	private Date createTime;

	/**
	 * 更新者
	 */
	@TableField(value = "update_by")
	private String updateBy;

	/**
	 * 更新时间
	 */
	@TableField(value = "update_time")
	private Date updateTime;


}
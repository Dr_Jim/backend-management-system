package com.example.humanresources.util.easyExcel.enums;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.stream.Stream;

@Getter
@AllArgsConstructor
public enum sexEnum {

    /**
     * 未知
     */
    UNKNOWN(0, "男"),

    /**
     * 男性
     */
    MALE(1, "女"),

    /**
     * 女性
     */
    FEMALE(2, "未知");

    private final Integer value;

    @JsonFormat
    private final String description;

    public static sexEnum convert(Integer value) {
        return Stream.of(values())
                .filter(bean -> bean.value.equals(value))
                .findAny()
                .orElse(UNKNOWN);
    }

    public static sexEnum convert(String description) {
        return Stream.of(values())
                .filter(bean -> bean.description.equals(description))
                .findAny()
                .orElse(UNKNOWN);
    }


}

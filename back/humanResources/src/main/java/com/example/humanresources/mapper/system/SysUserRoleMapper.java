package com.example.humanresources.mapper.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.humanresources.entity.system.SysUserRole;
import com.example.humanresources.vo.system.SysUserMergeVo;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.lang.reflect.Array;
import java.util.ArrayList;

/**
* @author Jim
* @description 针对表【sys_user_role(用户和角色关联表)】的数据库操作Mapper
* @createDate 2024-01-12 20:57:23
* @Entity generator.domain.SysUserRole
*/

@Mapper
public interface SysUserRoleMapper extends BaseMapper<SysUserRole> {

    public int insertUserRole(@Param("userRoleList") ArrayList<SysUserRole> userRoleList);

}





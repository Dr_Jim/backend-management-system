package com.example.humanresources.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysDictData;
import com.example.humanresources.vo.system.SysDictDataQueryVo;

import java.util.List;

/**
* @author Jim
* @description 针对表【sys_dict_data(字典数据表)】的数据库操作Service
* @createDate 2024-01-03 20:14:48
*/
public interface SysDictDataService extends IService<SysDictData> {
    /**
     * 根据条件分页查询字典数据
     *
     * @param sysDictDataQueryVo 字典数据信息
     * @return 字典数据集合信息
     */
    public List<SysDictData> selectDictDataList(SysDictDataQueryVo sysDictDataQueryVo);
}

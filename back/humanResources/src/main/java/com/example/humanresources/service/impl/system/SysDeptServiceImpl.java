package com.example.humanresources.service.impl.system;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.example.humanresources.entity.system.SysDept;
import com.example.humanresources.mapper.system.SysDeptMapper;
import com.example.humanresources.service.system.SysDeptService;
import com.example.humanresources.vo.system.SysDeptMergeVo;
import com.example.humanresources.vo.system.SysDeptQueryVo;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

/**
* @author Jim
* @description 针对表【sys_dept(组织机构)】的数据库操作Service实现
* @createDate 2023-11-24 21:42:14
*/
@Service
public class SysDeptServiceImpl extends ServiceImpl<SysDeptMapper, SysDept> implements SysDeptService {

    @Resource
    SysDeptMapper sysDeptMapper;

    /**
     *  查询所有部门信息
     * @param sysDeptQueryVo 用户部门信息
     * @return List
     */
    @Override
    public List<SysDept> selectDeptInfo(SysDeptQueryVo sysDeptQueryVo) {
        return sysDeptMapper.selectDeptInfo(sysDeptQueryVo);
    }

    /**
     * 删除部门信息
     * @param deleteId
     * @return int
     */
    @Override
    public int deleteDeptInfo(String deleteId) {
        return sysDeptMapper.deleteDeptInfo(deleteId);
    }

    /**
     * 添加部门信息
     * @param sysDeptMergeVo 用户部门信息
     * @return List
     */
    @Override
    public int insertDeptInfo(SysDeptMergeVo sysDeptMergeVo) {
//        如果存在deptId则证明这条数据是子级,同时需要查询其ancestor
        Long deptId = sysDeptMergeVo.getDeptId();
        if(deptId != null){
            sysDeptMergeVo.setParentId(deptId);
//            Long ancestors = sysDeptMergeVo.getAncestors();
//            sysDeptMergeVo.setAncestors(ancestors);
        }
        return sysDeptMapper.insertDeptInfo(sysDeptMergeVo);
    }

    /**
     * 更新部门信息
     * @param sysDeptMergeVo
     * @return int
     */
    @Override
    public int updateDeptInfo(SysDeptMergeVo sysDeptMergeVo) {
        return sysDeptMapper.updateDeptInfo(sysDeptMergeVo);
    }
}





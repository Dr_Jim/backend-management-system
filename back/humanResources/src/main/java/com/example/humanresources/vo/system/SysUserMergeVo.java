package com.example.humanresources.vo.system;


import com.alibaba.excel.annotation.ExcelProperty;
import com.alibaba.excel.annotation.write.style.ColumnWidth;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.example.humanresources.util.easyExcel.customConverter.sexConverter;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 添加和更新的vo类
 */

@Data
public class SysUserMergeVo implements Serializable {

    private static final long serialVersionUID = 1L;


    /**
     * 岗位id
     */
    @TableField(exist = false)
    private List<Long> postIds;

    /**
     * 角色id
     */
    @TableField(exist = false)
    private List<Long> roleIds;

    /**
     * 用户ID
     */
    @TableId(value = "user_id", type = IdType.AUTO)
    private Long userId;

    /**
     * 部门ID
     */
    @TableField(value = "dept_id")
    private Long deptId;

    /**
     * 用户账号
     */
    @ExcelProperty("用户账号")
    @ColumnWidth(20)
    @TableField(value = "user_name")
    private String userName;

    /**
     * 用户昵称
     */
    @ExcelProperty("用户昵称")
    @ColumnWidth(20)
    @TableField(value = "nick_name")
    private String nickName;

    /**
     * 用户邮箱
     */
    @ExcelProperty("用户邮箱")
    @ColumnWidth(20)
    @TableField(value = "email")
    private String email;

    /**
     * 手机号码
     */
    @ExcelProperty("手机号码")
    @ColumnWidth(20)
    @TableField(value = "phonenumber")
    private String phonenumber;

    /**
     * 用户性别（0男 1女 2未知）
     */
    @ExcelProperty(value = "用户性别", converter = sexConverter.class)
    @ColumnWidth(20)
    @TableField(value = "sex")
    private String sex;

    /**
     * 密码
     */
    @TableField(value = "password")
    private String password;

    /**
     * 备注
     */
    @TableField(value = "remark")
    private String remark;

    /**
     * 帐号状态（0正常 1停用）
     */
    @TableField(value = "status")
    private String status;


}

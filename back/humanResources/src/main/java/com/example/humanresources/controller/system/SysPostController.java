package com.example.humanresources.controller.system;

import com.example.humanresources.config.PageBean;
import com.example.humanresources.entity.system.SysPost;
import com.example.humanresources.service.system.SysPostService;
import com.example.humanresources.util.Result;
import com.example.humanresources.vo.system.SysPostQueryVo;
import com.github.pagehelper.PageHelper;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@RestController
@RequestMapping("/system/post")
public class SysPostController {

    @Resource
    private SysPostService sysPostQueryVoService;


    @ApiOperation("查询岗位信息")
    @PostMapping("selectPostInfo")
    public Result selectPostInfo(SysPostQueryVo sysPostQueryVo){
        // 设置分页必填参数
        if (sysPostQueryVo.getPage() == null){
            sysPostQueryVo.setPage(1);
        }
        if (sysPostQueryVo.getLimit() == null){
            sysPostQueryVo.setLimit(10);
        }
        // 设置分页
        PageHelper.startPage(sysPostQueryVo.getPage(), sysPostQueryVo.getLimit());
        List<SysPost> list = sysPostQueryVoService.selectPostInfo(sysPostQueryVo);
        PageBean<SysPost> result = new PageBean<>(list);
        return Result.ok(result);
    }
}

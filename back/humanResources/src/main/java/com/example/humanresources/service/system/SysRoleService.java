package com.example.humanresources.service.system;

import com.baomidou.mybatisplus.extension.service.IService;
import com.example.humanresources.entity.system.SysRole;
import com.example.humanresources.vo.system.SysRoleMergeVo;
import com.example.humanresources.vo.system.SysRoleQueryVo;

import java.util.List;


/**
* @author Jim
* @description 针对表【sys_role(角色信息表)】的数据库操作Service
* @createDate 2024-01-02 08:56:15
*/
public interface SysRoleService extends IService<SysRole> {
    int insertRoleInfo(SysRoleMergeVo sysRoleMergeVo);

    List<SysRole> selectRoleInfo(SysRoleQueryVo sysRoleQueryVo);

    int deleteRoleInfo(String deleteId);

    int updateRoleInfo(SysRoleMergeVo sysRoleMergeVo);
}

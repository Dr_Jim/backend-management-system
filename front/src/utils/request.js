import axios from 'axios'
import store from '@/store'
import { Message, Loading } from 'element-ui'
// import router from '@/router'
import { saveAs } from 'file-saver'
import errorCode from './errorCode'
import { tansParams, blobValidate } from '@/utils/ruoyi'

// 定义一个下载实例
let downloadLoadingInstance

const service = axios.create({
  baseURL: process.env.VUE_APP_BASE_API, // 基础地址
  timeout: 100000
}) // 创建一个新的axios实例
// 成功1 失败2
service.interceptors.request.use((config) => {
  // 注入token
//  this.$store.getters
  // store.getters.token => 请求头里面
  if (store.getters.token) {
    config.headers.Authorization = `Bearer ${store.getters.token}`
  }
  return config
}, (error) => {
  // 失败执行promise
  return Promise.reject(error)
})

// 响应拦截器
service.interceptors.response.use((response) => {
  // axios默认包裹了data
  // 判断是不是Blob
  if (response.data instanceof Blob) return response.data // 返回了Blob对象
  const { data, msg, code } = response.data // 默认json格式
  // console.log(response.data)
  if (code === '200') {
    return data
  } else {
    Message({ type: 'error', msg })
    return Promise.reject(new Error(msg))
  }
}, async(error) => {
  if (error.response.status === 401) {
    // Message({ type: 'warning', message: 'token超时了' })
    // // 说明token超时了
    // await store.dispatch('user/logout') // 调用action 退出登录
    // //  主动跳到登录页
    // router.push('/login') // 跳转到登录页
    // return Promise.reject(error)
  }
  // error.message
  Message({ type: 'error', message: error.message })
  return Promise.reject(error)
})

// 通用下载方法
export function download(url, params, filename, config) {
  downloadLoadingInstance = Loading.service({ text: '正在下载数据，请稍候', spinner: 'el-icon-loading', background: 'rgba(0, 0, 0, 0.7)' })
  return service.post(url, params, {
    transformRequest: [(params) => { return tansParams(params) }],
    headers: { 'Content-Type': 'application/x-www-form-urlencoded' },
    responseType: 'blob',
    ...config
  }).then(async(data) => {
    const isBlob = blobValidate(data)
    if (isBlob) {
      const blob = new Blob([data])
      saveAs(blob, filename)
    } else {
      const resText = await data.text()
      const rspObj = JSON.parse(resText)
      const errMsg = errorCode[rspObj.code] || rspObj.msg || errorCode['default']
      Message.error(errMsg)
    }
    downloadLoadingInstance.close()
  }).catch((r) => {
    console.error(r)
    Message.error('下载文件出现错误，请联系管理员！')
    downloadLoadingInstance.close()
  })
}

export default service

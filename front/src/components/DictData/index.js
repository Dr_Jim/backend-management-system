import Vue from 'vue'
import store from '@/store'
import DataDict from '@/utils/dict'
import { listData } from '@/api/system/dict/data'

function searchDictByKey(dict, key) {
  if (key == null && key === '') {
    return null
  }
  try {
    for (let i = 0; i < dict.length; i++) {
      if (dict[i].key === key) {
        return dict[i].value
      }
    }
  } catch (e) {
    return null
  }
}

function install() {
// 把DataDict当作一个插件安装到Vue实例中
  Vue.use(DataDict, {
    // 数据字典元信息
    metas: {
      // '*'表示这是一个通用配置，适用于所有数据字典
      '*': {
        // labelField 和 valueField 是用于指定数据字典项中标签和值的字段名
        labelField: 'dictLabel',
        valueField: 'dictValue',
        // 这个函数用来请求数据字典的数据，调用 getDicts() 函数，并使用传入的 dictMeta.type 来获取相应类型的数据字典
        request(dictMeta) {
          const storeDict = searchDictByKey(store.getters.dict, dictMeta.type)
          if (storeDict) {
            return new Promise(resolve => { resolve(storeDict) })
          } else {
            return new Promise((resolve, reject) => {
              // console.log(dictMeta.type)
              listData({
                page: 1,
                limit: 10,
                dictType: dictMeta.type
              }).then(res => {
                // console.log(res)
                store.dispatch('dict/setDict', { key: dictMeta.type, value: res })
                console.log(store.getters.dict)
                resolve(res)
              }).catch(error => {
                reject(error)
              })
            })
          }
        }
      }
    }
  })
}

export default {
  install
}

import request from '@/utils/request'
// import { parseStrEmpty } from '@/utils/ruoyi'

// 查询岗位列表
export function selectPostInfo(data) {
  return request({
    url: '/system/post/selectPostInfo',
    method: 'post',
    data: data
  })
}

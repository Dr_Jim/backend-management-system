import request from '@/utils/request'

/** *
 * 获取角色列表
 * **/
export function getRoleList(data) {
  return request({
    url: '/system/role/selectRole',
    method: 'post',
    data // 查询参数
  })
}

/** **
 * 新增角色
 * ***/

export function addRole(data) {
  return request({
    url: '/system/role/insertRole',
    method: 'post',
    data
  })
}

/**
 * 更新角色
 * ***/

export function updateRole(data) {
  return request({
    url: '/system/role/updateRole',
    method: 'post',
    data
  })
}

/** *
 * 删除角色
 * **/

export function delRole(id) {
  return request({
    url: `/system/role/deleteRole/${id}`,
    method: 'post'
  })
}

/**
 * 获取角色详情
 * **/

export function getRoleDetail(id) {
  return request({
    url: `/sys/role/${id}`
  })
}

/**
 * 给角色分配权限
 *
 * ***/

export function assignPerm(data) {
  return request({
    url: '/sys/role/assignPrem',
    method: 'put',
    data
  })
}

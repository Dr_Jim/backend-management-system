import request from '@/utils/request'

/**
 *
 * 获取组织架构数据
 *
 */
export function getDepartment(data) {
  return request({
    method: 'post',
    url: '/system/dept/selectDept',
    data
  })
}

// /**
//  * 获取部门详情
//  *
//  * ***/
//
// export function getDepartmentDetail(id) {
//   return request({
//     url: `/api/system/dept/detail/${id}`
//   })
// }

/**
 *
 *  获取部门负责人的数据
 * **/
//
// export function getManagerList() {
//   return request({
//     url: '/system/dept/selectDept'
//   })
// }

/**
 * 新增组织
 * ***/
export function addDepartment(data) {
  return request({
    method: 'post',
    url: '/system/dept/insertDept',
    data
  })
}

/** *
 * 更新部门
 * ***/
export function updateDepartment(data) {
  return request({
    method: 'post',
    url: '/system/dept/updateDept',
    data
  })
}

/**
 * 删除部门
 *
 */

export function delDepartment(id) {
  return request({
    method: 'post',
    url: `/system/dept/deleteDept/${id}`
  })
}

import layout from '@/layout'
export default {
  path: '/system',
  component: layout,
  redirect: '/system/menu1',
  name: 'system',
  meta: {
    title: '系统管理',
    icon: 'setting'
  },
  children: [
    // {
    //   path: 'menu1',
    //   component: () => import('@/views/nested/menu1/index'), // Parent router-view
    //   name: 'Menu1',
    //   meta: { title: 'Menu1' },
    //   children: [
    //     {
    //       path: 'menu1-1',
    //       component: () => import('@/views/nested/menu1/menu1-1'),
    //       name: 'Menu1-1',
    //       meta: { title: 'Menu1-1' }
    //     },
    //     {
    //       path: 'menu1-2',
    //       component: () => import('@/views/nested/menu1/menu1-2'),
    //       name: 'Menu1-2',
    //       meta: { title: 'Menu1-2' },
    //       children: [
    //         {
    //           path: 'menu1-2-1',
    //           component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
    //           name: 'Menu1-2-1',
    //           meta: { title: 'Menu1-2-1' }
    //         },
    //         {
    //           path: 'menu1-2-2',
    //           component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
    //           name: 'Menu1-2-2',
    //           meta: { title: 'Menu1-2-2' }
    //         }
    //       ]
    //     },
    //     {
    //       path: 'menu1-3',
    //       component: () => import('@/views/nested/menu1/menu1-3'),
    //       name: 'Menu1-3',
    //       meta: { title: 'Menu1-3' }
    //     }
    //   ]
    // },
    {
      path: '/user',
      name: 'user',
      component: () => import('@/views/user'),
      meta: {
        title: '用户管理',
        icon: 'people'
      }
    },
    {
      path: '/employee/detail/:id?', // 员工详情的地址
      component: () => import('@/views/employee/detail.vue'),
      hidden: true, // 表示隐藏在左侧菜单
      meta: {
        title: '员工详情' // 显示在导航的文本
      }
    },
    {
      path: '/department',
      component: () => import('@/views/department'),
      name: 'department',
      meta: {
        // 路由元信息 存储数据的
        icon: 'tree', // 图标
        title: '部门管理' // 标题
      }
    },
    {
      path: '/role',
      component: () => import('@/views/role'),
      name: 'role',
      meta: {
        title: '角色管理',
        icon: 'setting'
      }
    }
  ]
}

// import Vue from 'vue'
// import Router from 'vue-router'
//
// Vue.use(Router)
//
// /* Layout */
// import Layout from '@/layout'
//
// /**
//  * Note: sub-menu only appear when route children.length >= 1
//  * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
//  *
//  * hidden: true                   if set true, item will not show in the sidebar(default is false)
//  * alwaysShow: true               if set true, will always show the root menu
//  *                                if not set alwaysShow, when item has more than one children route,
//  *                                it will becomes nested mode, otherwise not show the root menu
//  * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
//  * name:'router-name'             the name is used by <keep-alive> (must set!!!)
//  * meta : {
//     roles: ['admin','editor']    control the page roles (you can set multiple roles)
//     title: 'title'               the name show in sidebar and breadcrumb (recommend set)
//     icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
//     breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
//     activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
//   }
//  */
//
// /**
//  * constantRoutes
//  * a base page that does not have permission requirements
//  * all roles can be accessed
//  */
// export const constantRoutes = [
//   {
//     path: '/login',
//     component: () => import('@/views/login/index'),
//     hidden: true
//   },
//
//   {
//     path: '/404',
//     component: () => import('@/views/404'),
//     hidden: true
//   },
//
//   {
//     path: '/',
//     component: Layout,
//     redirect: '/dashboard',
//     children: [{
//       path: 'dashboard',
//       name: 'Dashboard',
//       component: () => import('@/views/dashboard/index'),
//       meta: { title: 'Dashboard', icon: 'dashboard' }
//     }]
//   },
//
//   {
//     path: '/system',
//     name: 'system',
//     component: Layout,
//     redirect: '/employee/employee',
//     meta: {
//       title: '系统管理',
//       icon: 'setting'
//     },
//     children: [
//       {
//         path: '/employee',
//         // component: Layout,
//         name: 'employee',
//         children: [{
//           path: '',
//           name: 'employee',
//           component: () => import('@/views/employee'),
//           meta: {
//             title: '员工',
//             icon: 'people'
//           }
//         }, {
//           path: '/employee/detail/:id?', // 员工详情的地址
//           component: () => import('@/views/employee/detail.vue'),
//           hidden: true, // 表示隐藏在左侧菜单
//           meta: {
//             title: '员工详情' // 显示在导航的文本
//           }
//         }]
//       },
//       {
//         // 路由信息
//         path: '/department',
//         // component: Layout, // 一级路由
//         name: 'department',
//         children: [{
//           path: '', // 二级路由地址为空时 表示 /department 显示一级路由 + 二级路由
//           component: () => import('@/views/department'),
//           name: 'department', // 可以用来跳转 也可以标记路由
//           meta: {
//             // 路由元信息 存储数据的
//             icon: 'tree', // 图标
//             title: '组织' // 标题
//           }
//         }]
//       },
//       {
//         // 路由信息
//         path: '/department',
//         // component: Layout, // 一级路由
//         name: 'department',
//         children: [{
//           path: '', // 二级路由地址为空时 表示 /department 显示一级路由 + 二级路由
//           component: () => import('@/views/department'),
//           name: 'setting', // 可以用来跳转 也可以标记路由
//           meta: {
//             // 路由元信息 存储数据的
//             icon: 'setting', // 图标
//             title: '系统管理' // 标题
//           }
//         }]
//       }
//     ]
//   },
//
//   // {
//   //   path: '/example',
//   //   component: Layout,
//   //   redirect: '/example/table',
//   //   name: 'Example',
//   //   meta: { title: 'Example', icon: 'el-icon-s-help' },
//   //   children: [
//   //     {
//   //       path: 'table',
//   //       name: 'Table',
//   //       component: () => import('@/views/table/index'),
//   //       meta: { title: 'Table', icon: 'table' }
//   //     },
//   //     {
//   //       path: 'tree',
//   //       name: 'Tree',
//   //       component: () => import('@/views/tree/index'),
//   //       meta: { title: 'Tree', icon: 'tree' }
//   //     }
//   //   ]
//   // },
//
//   // {
//   //   path: '/form',
//   //   component: Layout,
//   //   children: [
//   //     {
//   //       path: 'index',
//   //       name: 'Form',
//   //       component: () => import('@/views/form/index'),
//   //       meta: { title: 'Form', icon: 'form' }
//   //     }
//   //   ]
//   // },
//   //
//   // {
//   //   path: '/nested',
//   //   component: Layout,
//   //   redirect: '/nested/menu1',
//   //   name: 'Nested',
//   //   meta: {
//   //     title: 'Nested',
//   //     icon: 'nested'
//   //   },
//   //   children: [
//   //     {
//   //       path: 'menu1',
//   //       component: () => import('@/views/nested/menu1/index'), // Parent router-view
//   //       name: 'Menu1',
//   //       meta: { title: 'Menu1' },
//   //       children: [
//   //         {
//   //           path: 'menu1-1',
//   //           component: () => import('@/views/nested/menu1/menu1-1'),
//   //           name: 'Menu1-1',
//   //           meta: { title: 'Menu1-1' }
//   //         },
//   //         {
//   //           path: 'menu1-2',
//   //           component: () => import('@/views/nested/menu1/menu1-2'),
//   //           name: 'Menu1-2',
//   //           meta: { title: 'Menu1-2' },
//   //           children: [
//   //             {
//   //               path: 'menu1-2-1',
//   //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-1'),
//   //               name: 'Menu1-2-1',
//   //               meta: { title: 'Menu1-2-1' }
//   //             },
//   //             {
//   //               path: 'menu1-2-2',
//   //               component: () => import('@/views/nested/menu1/menu1-2/menu1-2-2'),
//   //               name: 'Menu1-2-2',
//   //               meta: { title: 'Menu1-2-2' }
//   //             }
//   //           ]
//   //         },
//   //         {
//   //           path: 'menu1-3',
//   //           component: () => import('@/views/nested/menu1/menu1-3'),
//   //           name: 'Menu1-3',
//   //           meta: { title: 'Menu1-3' }
//   //         }
//   //       ]
//   //     },
//   //     {
//   //       path: 'menu2',
//   //       component: () => import('@/views/nested/menu2/index'),
//   //       name: 'Menu2',
//   //       meta: { title: 'menu2' }
//   //     }
//   //   ]
//   // },
//   //
//   // {
//   //   path: 'external-link',
//   //   component: Layout,
//   //   children: [
//   //     {
//   //       path: 'https://panjiachen.github.io/vue-element-admin-site/#/',
//   //       meta: { title: 'External Link', icon: 'link' }
//   //     }
//   //   ]
//   // },
//
//   // 404 page must be placed at the end !!!
//   { path: '*', redirect: '/404', hidden: true }
// ]
//
// const createRouter = () => new Router({
//   // mode: 'history', // require service support
//   scrollBehavior: () => ({ y: 0 }),
//   routes: constantRoutes
// })
//
// const router = createRouter()
//
// // Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
// export function resetRouter() {
//   const newRouter = createRouter()
//   router.matcher = newRouter.matcher // reset router
// }
//
// export default router

// 动态写法

import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

/* Layout */
import Layout from '@/layout'
// import departmentRouter from './modules/department'
// import approvalRouter from './modules/approval'
// import attendanceRouter from './modules/attendance'
// import employeeRouter from './modules/employee'
// import permissionRouter from './modules/permission'
// import roleRouter from './modules/role'
import systemRouter from './modules/system'
// import salaryRouter from './modules/salary'
// import socialRouter from './modules/social'

/**
 * Note: sub-menu only appear when route children.length >= 1
 * Detail see: https://panjiachen.github.io/vue-element-admin-site/guide/essentials/router-and-nav.html
 *
 * hidden: true                   if set true, item will not show in the sidebar(default is false)
 * alwaysShow: true               if set true, will always show the root menu
 *                                if not set alwaysShow, when item has more than one children route,
 *                                it will becomes nested mode, otherwise not show the root menu
 * redirect: noRedirect           if set noRedirect will no redirect in the breadcrumb
 * name:'router-name'             the name is used by <keep-alive> (must set!!!)
 * meta : {
 roles: ['admin','editor']    control the page roles (you can set multiple roles)
 title: 'title'               the name show in sidebar and breadcrumb (recommend set)
 icon: 'svg-name'/'el-icon-x' the icon show in the sidebar
 breadcrumb: false            if set false, the item will hidden in breadcrumb(default is true)
 activeMenu: '/example/list'  if set path, the sidebar will highlight the path you set
 }
 */

/**
 * constantRoutes
 * a base page that does not have permission requirements
 * all roles can be accessed
 */
export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: { title: '首页', icon: 'dashboard' }
    }]
  }

  // 404 page must be placed at the end !!!

]
// 动态路由
export const asyncRoutes = [
  // departmentRouter,
  // roleRouter,
  // employeeRouter,
  systemRouter
  // permissionRouter,
  // attendanceRouter,
  // approvalRouter,
  // salaryRouter,
  // socialRouter
]
const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({ y: 0 }),
  routes: [...constantRoutes, ...asyncRoutes] // 默认引入静态路由
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
